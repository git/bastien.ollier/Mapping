# Mapping

Cette application en Kotlin permet aux utilisateurs de rechercher les restaurants proches et de visualiser des informations sur les restaurants. Elle utilise l'API de tomtom.com pour trouver les restaurants ainsi que de mapbox.com pour faire la carte.

## Fonctionnalité
- Recherche les restaurants dans rayon de 1km
- Consultation des informations sur les restaurants
- Possibilité de liker un restaurant pour y a accéder plus rapidement via une liste dédiée

## Utilisation/Navigation

### carte
<img src="imageReadme/carte.png" width="216" height="480" />

Appuyer sur le bouton avec la loupe pour faire une rechercher des restaurants proches

### liste des restaurants trouver
<img src="imageReadme/listeRestaurants.png" width="216" height="480" />

Appuyer sur l'icon d'un restaurant pour optenir les détails

### informations sur un restaurants
<img src="imageReadme/info.png" width="216" height="480" />

Appuyer sur le bouton pour mettre le restaurant en favorie


### Liste des restaurants favoris
<img src="imageReadme/favorie.png" width="216" height="480" />

Consulter les informations sur les restaurants en favoris

## Contributeurs
- Bastien Ollier PM2 (100% du projet)
- Clement Verdoire WEB2 (0% du projet)