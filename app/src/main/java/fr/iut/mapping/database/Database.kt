package fr.iut.mapping.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.iut.mapping.database.DAO.RestaurantDAO
import fr.iut.mapping.database.Entity.RestaurantEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(RestaurantEntity::class), version = 1, exportSchema = false)
abstract class RestaurantDatabase : RoomDatabase() {

    abstract fun restaurantDAO(): RestaurantDAO

    companion object {
        @Volatile
        private var INSTANCE: RestaurantDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): RestaurantDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext, RestaurantDatabase::class.java, "restaurant_database"
                ).addCallback(RestaurantDatabaseCallback(scope)).build()
                INSTANCE = instance
                instance
            }
        }
    }

    private class RestaurantDatabaseCallback(private val scope: CoroutineScope) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.restaurantDAO())
                }
            }
        }

        suspend fun populateDatabase(restaurantDao: RestaurantDAO) {
            // Delete all content here.
            restaurantDao.deleteAll()

            // Add sample words.
            //var resto = RestaurantEntity(0,0.0,0.0,"SUPER RESTO","0000","11 rue")
            //restaurantDao.insert(resto)
            //resto = RestaurantEntity(0,1.0,1.0,"SUPER RESTO2","00002","22 rue")
            //restaurantDao.insert(resto)

        }
    }



}
