package fr.iut.mapping.database.DAO

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.iut.mapping.database.Entity.RestaurantEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface RestaurantDAO {

    @Query("SELECT *  FROM restaurant_table")
    fun getAllRestaurants(): Flow<List<RestaurantEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(Restaurant: RestaurantEntity)

    @Query("DELETE FROM restaurant_table")
    suspend fun deleteAll()

}