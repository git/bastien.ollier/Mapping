package fr.iut.mapping.database

import androidx.annotation.WorkerThread
import fr.iut.mapping.database.DAO.RestaurantDAO
import fr.iut.mapping.database.Entity.RestaurantEntity
import kotlinx.coroutines.flow.Flow

class RestaurantRepository(private val restaurantDAO: RestaurantDAO) {

    val restaurantLikes: Flow<List<RestaurantEntity>> = restaurantDAO.getAllRestaurants()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(restaurant: RestaurantEntity) {
        restaurantDAO.insert(restaurant)
    }

}