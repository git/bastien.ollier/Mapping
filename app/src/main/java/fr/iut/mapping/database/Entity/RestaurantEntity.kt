package fr.iut.mapping.database.Entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "restaurant_table")
data class RestaurantEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val lat : Double,
    val lon : Double,
    val name: String,
    val phone:String?,
    val adress:String?) {
}