package fr.iut.mapping.API

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Query

private const val TOMTOM_API_PATH = "https://api.tomtom.com/search/2/";
val httpClient = OkHttpClient()


interface RestaurantAPI {

    @GET("nearbySearch/.json?key=s6oXlFd9IRM2vgPhNmzJ24HS2hlqcu00&categorySet=7315&limit=50")
    suspend fun getRestaurants(@Query("lat") lat: Double, @Query("lon") lon: Double, @Query("radius") radius: Int): NearbyPOI

}

fun createTomtomConnection(): Retrofit =
    Retrofit.Builder()
        .baseUrl(TOMTOM_API_PATH)
        .addConverterFactory(MoshiConverterFactory.create(
            Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()))
        .client(httpClient)
        .build()
