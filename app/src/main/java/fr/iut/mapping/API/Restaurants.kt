package fr.iut.mapping.API

import android.util.Log
import fr.iut.mapping.Model.RestaurantData
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking


fun getRestaurants(lat: Double, lon: Double, radius: Int):  List<RestaurantData> {
    var listRestaurant : List<RestaurantData> = emptyList()
     runBlocking {
        coroutineScope {
            val retrofit = createTomtomConnection()
            val apiInterface = retrofit.create(RestaurantAPI::class.java)

            try {
                val response = apiInterface.getRestaurants(lat, lon, radius)
                response.results.forEach {
                    listRestaurant += RestaurantData(it.position.lat,it.position.lon,it.poi.name,it.poi.phone,it.address.freeformAddress)
                }

            } catch (Ex: Exception) {
                Log.e("Error", Ex.toString())
            }
        }
    }

    return listRestaurant
}
