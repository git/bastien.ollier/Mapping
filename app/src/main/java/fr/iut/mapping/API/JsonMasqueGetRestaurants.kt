package fr.iut.mapping.API

data class NearbyPOI(
    val results: List<Result>
)

data class Result(
    val type: String,
    val id: String,
    val score: Double,
    val dist: Double,
    val info: String,
    val poi: POI,
    val address: Address,
    val position: Position,
)

data class POI(
    val name: String,
    val phone: String?,
)

data class Address(
    val freeformAddress: String,
    val localName: String
)

data class Position(
    val lat: Double,
    val lon: Double
)
