package fr.iut.mapping

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.iut.mapping.Model.RestaurantData

class RestaurantAdapter : ListAdapter<RestaurantData, RestaurantAdapter.RestaurantViewHolder>(RESTAURANT_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        return RestaurantViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.name,current.adress,current.phone,current.lat,current.lon)
    }

    override fun getItemCount(): Int {
        return currentList.size
    }

    class RestaurantViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val restaurantName: TextView = itemView.findViewById(R.id.nameRestaurant)
        private val restaurantAddress: TextView = itemView.findViewById(R.id.addressRestaurant)
        private val restauranttel: TextView = itemView.findViewById(R.id.telephoneRestaurant)
        private val restaurantGps: TextView = itemView.findViewById(R.id.GPSRestaurant)

        fun bind(name: String,address: String?,tel:String?,lat: Double, lon: Double ) {

            restaurantName.text = name
            restaurantAddress.text = address
            restauranttel.text = tel
            restaurantGps.text = "${lat},${lon}"
        }

        companion object {
            fun create(parent: ViewGroup): RestaurantViewHolder {
                val view: View = LayoutInflater.from(parent.context).inflate(R.layout.recyclerviewitem, parent, false)
                return RestaurantViewHolder(view)
            }
        }
    }

    companion object {
        private val RESTAURANT_COMPARATOR = object : DiffUtil.ItemCallback<RestaurantData>() {
            override fun areItemsTheSame(oldItem: RestaurantData, newItem: RestaurantData): Boolean = oldItem === newItem

            override fun areContentsTheSame(oldItem: RestaurantData, newItem: RestaurantData): Boolean =
                        oldItem.name == newItem.name &&
                        oldItem.lat == newItem.lat &&
                        oldItem.lon == newItem.lon &&
                        oldItem.adress == newItem.adress &&
                        oldItem.phone == newItem.phone

        }
    }
}
