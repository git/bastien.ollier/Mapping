package fr.iut.mapping

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.JsonParser
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.utils.BitmapUtils
import fr.iut.mapping.Model.RestaurantData
import fr.iut.mapping.API.getRestaurants
import kotlinx.android.synthetic.main.fragment_map_page.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FirstFragment: Fragment(R.layout.fragment_map_page) {
    companion object {
        private const val MARKER_SELECTED_ICON = "MARKER_SELECTED_ICON"
        private const val MARKER_ICON = "MARKER_ICON"
    }

    private var mapView: MapView? = null
    private var symbolManager: SymbolManager? = null
    private var lastSymbol: Symbol? = null

    private lateinit var locationListener: fr.iut.mapping.LocationListener
    private lateinit var listRestaurant: List<RestaurantData>

    private lateinit var currentRestaurant: RestaurantData

    private fun makeStyleUrl(): String {
        return getString(R.string.mapbox_style_url);
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreate(savedInstanceState)

        locationListener = LocationListener(requireContext())
        locationListener.start()

        Mapbox.getInstance(requireContext(), R.string.mapbox_access_token.toString())
        val rootView = inflater.inflate(R.layout.fragment_map_page, container, false)

        val button: FloatingActionButton = rootView.findViewById(R.id.findRestaurant)
        button.setOnClickListener {
            findAndPutRestaurant(rootView)
        }


        mapView = rootView.findViewById(R.id.mapView)
        mapView?.onCreate(savedInstanceState)

        mapView?.getMapAsync { map ->
            map.setStyle(makeStyleUrl()) {
                map.uiSettings.setAttributionMargins(15, 0, 0, 15)
            }
        }

        return rootView
    }

    private fun findAndPutRestaurant(rootView: View){
        locationListener.start()

        if(locationListener.asPermission()){
                Log.e("Debug","${locationListener.latitude},${locationListener.longitude}")
                listRestaurant = emptyList()
                listRestaurant = getRestaurants(locationListener.latitude,locationListener.longitude,1000)


                if(listRestaurant.isEmpty()){
                    Toast.makeText(requireContext(), "Pas de restaurants trouver", Toast.LENGTH_SHORT).show()
                }
                else {
                    mapView?.getMapAsync { map ->
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(locationListener.latitude, locationListener.longitude), 13.0))

                        map.setStyle(makeStyleUrl()) { style ->
                            map.uiSettings.setAttributionMargins(15, 0, 0, 15)

                            val selectedMarkerIconDrawable = ResourcesCompat.getDrawable(this.resources, R.drawable.ic_menu_likes, null)
                            style.addImage(MARKER_ICON, BitmapUtils.getBitmapFromDrawable(selectedMarkerIconDrawable)!!)

                            val markerIconDrawable = ResourcesCompat.getDrawable(this.resources, R.drawable.ic_menu_likes, null)
                            style.addImage(MARKER_SELECTED_ICON, BitmapUtils.getBitmapFromDrawable(markerIconDrawable)!!)

                            this.symbolManager = SymbolManager(mapView!!, map, style)
                            this.symbolManager?.iconAllowOverlap = true
                            this.symbolManager?.iconIgnorePlacement = true

                            listRestaurant.forEach { restaurant ->
                                restaurant.adress?.let {
                                    insertIconOnMap(LatLng(restaurant.lat, restaurant.lon), restaurant.name,
                                        it,restaurant.phone ?: "Pas de numéro disponible")
                                }
                            }

                            this.symbolManager?.addClickListener {
                                val clickedData = it.data?.asJsonObject
                                titleView.text = clickedData?.get("title")?.asString
                                addressRestaurant.text = clickedData?.get("address")?.asString
                                telephoneRestaurant.text = clickedData?.get("phone")?.asString
                                GPSRestaurant.text = clickedData?.get("GPS")?.asString

                                val button: Button = rootView.findViewById(R.id.addToFav)
                                button.setOnClickListener {
                                    Log.d("debug button","click")
                                    Log.d("debug button",clickedData.toString())
                                    val gps = (clickedData?.get("GPS")?.asString ?: "").split(",")
                                    val lat = gps[0]
                                    val lon = gps[1]

                                    val name = clickedData?.get("title")?.asString ?: ""
                                    val address = clickedData?.get("address")?.asString ?: ""
                                    val phone = clickedData?.get("phone")?.asString ?: ""
                                    val restaurant = RestaurantData(lat.toDouble(), lon.toDouble(), name,address,phone)

                                    val restaurantViewModel = ViewModelProvider(this, RestaurantViewModelFactory((requireActivity().application as MappingApplication).repository)).get(RestaurantViewModel::class.java)
                                    restaurantViewModel.insert(restaurant)
                                }

                                setSelectedIcon(it)
                                toggleLayout()
                                true
                            }
                        }
                    }
                }
        }else{
            Toast.makeText(requireContext(), "Pas permission pour le gps", Toast.LENGTH_SHORT).show()
        }
    }


    private fun toggleLayout() {
        mapLayout?.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 2f)
        descriptionLayout?.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1f)
    }

    private fun setDefaultIcon(symbol: Symbol) {
        symbol.iconImage = MARKER_ICON
        symbol.iconSize = 2f
        symbolManager?.update(symbol)
    }

    private fun setSelectedIcon(symbol: Symbol) {
        symbol.iconImage = MARKER_SELECTED_ICON
        symbol.iconSize = 2f
        symbolManager?.update(symbol)

        if (this.lastSymbol != null) {
            setDefaultIcon(this.lastSymbol!!)
        }
        this.lastSymbol = symbol
    }

    private fun insertIconOnMap(point: LatLng, title: String, address: String,phone: String) {
        val lat = point.latitude
        val lon = point.longitude

        val jsonData = """
            {
                "title" : "$title",
                "address" : "$address",
                "phone" : "$phone",
                "GPS": "$lat,$lon"
            }
        """

        val newSymbol = symbolManager!!.create(
            SymbolOptions().withLatLng(LatLng(point.latitude, point.longitude)).withData(JsonParser.parseString(jsonData))
        )
        setDefaultIcon(newSymbol)
    }


    override fun onStart() {
        super.onStart()
        mapView?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView?.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
        locationListener.stop()
    }


}