package fr.iut.mapping


import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /*gps*/
        val locationPermissionRequest = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ) { permissions -> when {
                permissions.getOrDefault(ACCESS_FINE_LOCATION, false) -> {
                    Toast.makeText(this, "Permission fine location Granted", Toast.LENGTH_SHORT).show()
                }
                permissions.getOrDefault(ACCESS_COARSE_LOCATION, false) -> {
                    Toast.makeText(this, "Permission COARSE location Granted", Toast.LENGTH_SHORT).show()
                } else -> {
                    Toast.makeText(this, "Denied permission", Toast.LENGTH_SHORT).show()
                }
            }
        }
        locationPermissionRequest.launch(arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION))

        /*navBar*/
        val firstFragment=FirstFragment()
        val secondFragment=SecondFragment()

        setCurrentFragment(firstFragment)

        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.map->setCurrentFragment(firstFragment)
                R.id.likes->setCurrentFragment(secondFragment)
            }
            true
        }

    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment,fragment)
            commit()
        }

}