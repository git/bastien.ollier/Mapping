package fr.iut.mapping

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.mapping.Model.RestaurantData

class SecondFragment: Fragment(R.layout.fragment_likes_page) {

    private val restaurantViewModel: RestaurantViewModel by viewModels {
        RestaurantViewModelFactory((requireActivity().application as MappingApplication).repository)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_likes_page, container, false)

        val recyclerView = rootView.findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = RestaurantAdapter()
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())


        restaurantViewModel.restaurantLikes.observe(viewLifecycleOwner) { restaurantEntities ->
            val restaurantDataList: MutableList<RestaurantData?> =
                restaurantEntities.map { entity ->
                    RestaurantData(entity.lat, entity.lon, entity.name, entity.phone, entity.adress)
                }.toMutableList()
            adapter.submitList(restaurantDataList)
        }

        return rootView
    }
}