package fr.iut.mapping

import androidx.lifecycle.*
import fr.iut.mapping.Model.RestaurantData
import fr.iut.mapping.database.Entity.RestaurantEntity
import fr.iut.mapping.database.RestaurantRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RestaurantViewModel(private val repository: RestaurantRepository): ViewModel() {
    val restaurantLikes: LiveData<List<RestaurantEntity>> = repository.restaurantLikes.asLiveData()

    fun insert(restaurant: RestaurantData) = viewModelScope.launch {
        val resto = RestaurantEntity(0,restaurant.lat,restaurant.lon,restaurant.name,restaurant.phone,restaurant.adress)
        withContext(Dispatchers.IO) {
            repository.insert(resto)
        }
    }

}

class RestaurantViewModelFactory(private val repository: RestaurantRepository) : ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RestaurantViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RestaurantViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}