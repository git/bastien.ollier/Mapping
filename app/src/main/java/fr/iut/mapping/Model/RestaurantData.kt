package fr.iut.mapping.Model

data class RestaurantData(val lat : Double,
                          val lon : Double,
                          val name: String,
                          val phone:String?,
                          val adress:String?) {
}
