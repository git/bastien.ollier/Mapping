package fr.iut.mapping


import android.app.Application
import fr.iut.mapping.database.RestaurantDatabase
import fr.iut.mapping.database.RestaurantRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MappingApplication: Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { RestaurantDatabase.getDatabase(this,applicationScope) }
    val repository by lazy { RestaurantRepository(database.restaurantDAO()) }

}